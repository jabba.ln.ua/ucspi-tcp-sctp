#!/bin/sh


CURDIR=`pwd`

HDIR=/home/joker/.mta
VCSDIR=${CURDIR}/tcp

PKG=ucspi-tcp
VER=0.88

cu_vcs()
{
  svn commit -m "DEB auto update" ${HDIR}/qmail && svn update ${HDIR}/qmail
}

if [ "$1" = "patch" ]; then

  rm -f ${HDIR}/tnew/*
  cp -v ${VCSDIR}/* ${HDIR}/tnew

  diff -Naurp ${HDIR}/told ${HDIR}/tnew >${VCSDIR}/patches/ucspi-fade.patch
  #exit 0
fi

if [ "$2" = "vcs" ]; then

  cu_vcs
  #exit 0
fi

if [ -d ${PKG}-${VER} ]; then

  rm -Rf ${PKG}-${VER}/*

fi

if [ ! -d ${PKG}-${VER} ]; then

  mkdir -p ${PKG}-${VER}

fi


cp ${VCSDIR}/* ${PKG}-${VER}
cp -R ${VCSDIR}/patches ${PKG}-${VER}/patches
cp -R ${VCSDIR}/packaging/debian ${PKG}-${VER}/debian

cd ${PKG}-${VER}

find . -type d -name '.svn' -print0 | xargs -0 /bin/rm -R;

#exit 0

## DO IT - DEB

dpkg-buildpackage -rfakeroot -S -sa
echo "!!!!!!!!!!!!!!!      NEXT STEP    !!!!!!!!!!!!!!!!!!!!"
dpkg-buildpackage -rfakeroot

#dpkg -i ../${PKG}*.deb


cd ..
rm -R ${PKG}-${VER}

sudo dpkg -i ucspi*.deb
sudo rm -R /tmp/ucspi-tcp-sctp || exit 1

sudo build-ucspi-tcp-sctp || exit 1
sudo rm -R /tmp/ucspi-tcp-sctp
