#!/bin/sh

####

####
DEST=/home/joker/.mta/_build-rpm
SRC=tcp

PKG=ucspi-tcp-sctp
VER=0.88

####
check_dest_dir()
{

  if [ ! -d $PKG ]; then

    mkdir -p $PKG

  fi

}

#### check_dest_dir

####
##cp ${SRC}/* $PKG/

##cd $PKG
##find . -type d -name '.svn' -print0 | xargs -0 /bin/rm -R;
##cd ..

#### NEW

cp ${PKG}-src_${VER}.orig.tar.gz ${DEST}/SOURCES/${PKG}-src_${VER}.orig.tar.gz

cp ${SRC}/patches/*.diff ${DEST}/SOURCES
cp ${SRC}/patches/*.dif* ${DEST}/SOURCES
cp ${SRC}/patches/*.patch ${DEST}/SOURCES
cp ${SRC}/patches/centos/maketcprules ${DEST}/SOURCES

cp ${SRC}/packaging/centos/${PKG}.spec ${DEST}/SPECS


## build
echo "Build: rpmbuild -ba ${DEST}/SPECS/${PKG}.spec"
rpmbuild -ba ${DEST}/SPECS/${PKG}.spec
